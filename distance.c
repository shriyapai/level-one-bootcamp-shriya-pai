
#include <stdio.h>
#include<math.h>

float x1, y11, x2, y2;

void get_input()
{
    printf("Enter the  coordinate x1\n");
    scanf("%f", &x1);
    printf("Enter the coordinate y1\n");
    scanf("%f", &y11);
    printf("Enter the coordinate x2\n");
    scanf("%f", &x2);
    printf("Enter the coordinate y2\n");
    scanf("%f", &y2);
}

float compute()
{
    float d;
    d = sqrt((x2-x1)*(x2-x1)+(y2-y11)*(y2-y11));
    return d;
}

void output(float d)
{
    printf("Distance between points (%f,%f) and (%f,%f) is\n%f",x1,y11,x2,y2,d);
}

int main()
{
    float z;
    get_input();
    z=compute();
    output(z);
    
    return 0;
}

